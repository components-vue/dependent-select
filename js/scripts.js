Vue.component('vue-select', {
    props: ['list', 'label'],
    data: function () {
        return {
            value: undefined
        }
    },
    watch: {
        value(newValue){
            this.$emit('input', newValue)
        },
        list() {
        	this.value = undefined;
        }
    },
    template: `
        <select v-model="value">
            <option v-for="item of list" :value="item">{{label ? item[label] : label}}</option>
        </select>`
})

new Vue({
    el:"#app",
    data: {
        cat: undefined,
        subcat: undefined,
        subcategories: undefined,
        result: undefined
    },
    watch: {
        cat: function(newValue){
            const params = new URLSearchParams({cat_id: newValue}).toString()
            fetch(`http://localhost:3000/subcategories?${params}`)
                .then(r => r.json())
                .then(r => this.subcategories = r)
        }
    },
    methods: {
        submit(){
            this.result = {
                cat: this.cat,
                subcat: this.subcat
            }
        }
    }
})